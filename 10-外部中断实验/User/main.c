#include "system.h"
#include "SysTick.h"
#include "led.h"
#include "beep.h"
#include "key.h"
#include "exti.h"

int main()
{
    u8 i = 0;
    u8 toggle_counter = 0; // Counter to control buzzer frequency

    SysTick_Init(168);
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2); // Set priority group configuration
    LED_Init();
    KEY_Init();
    BEEP_Init();
    My_EXTI_Init(); // External interrupts initialization

    while(1)
    {
        i++;
        if(i % 20 == 0)
        {
            LED1 = !LED1; // Toggle LED every 200 ms
        }

        // Simple counter to simulate changing the pitch based on BEEP's state
        if (BEEP)
        {
            if (++toggle_counter >= 10) // Simulating higher pitch by toggling more frequently
            {
                BEEP = 0;
                toggle_counter = 0;
            }
        }
        else
        {
            if (++toggle_counter >= 50) // Simulating lower pitch by toggling less frequently
            {
                BEEP = 1;
                toggle_counter = 0;
            }
        }

        delay_ms(10);
    }
}
