/*******************************************************************************
*                 
*                 		       普中科技
--------------------------------------------------------------------------------
* 实 验 名		 : printf重定向
* 实验说明       : 
* 连接方式       : 
* 注    意		 : 
*******************************************************************************/

#include "system.h"
#include "SysTick.h"
#include "led.h"
#include "usart.h"	

/*******************************************************************************
* 函 数 名         : main
* 函数功能		   : 主函数
* 输    入         : 无
* 输    出         : 无
*******************************************************************************/
int main()
{
    u8 i, j;
    SysTick_Init(168);
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2); // Set interrupt priority grouping to group 2
    LED_Init();
    USART1_Init(115200);
    
    while(1)
    {
        for(i = 1; i <= 9; i++) // Outer loop for each row
        {
            for(j = 1; j <= i; j++) // Inner loop for each column
            {
                printf("%d*%d=%d\t", j, i, i*j); // Print the multiplication result
            }
            printf("\r\n"); // New line after each row
        }
        delay_ms(1000); // Wait for 1 second before reprinting the table
    }
}
