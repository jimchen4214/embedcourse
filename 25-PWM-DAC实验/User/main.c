#include "system.h"
#include "SysTick.h"
#include "led.h"
#include "usart.h"
#include "key.h"
#include "adc.h"
#include "pwm_dac.h"

/*******************************************************************************
* 函 数 名         : main
* 函数功能		   : 主函数
* 输    入         : 无
* 输    出         : 无
*******************************************************************************/
int main()
{	
	u8 i=0;
	u8 key;
	int pwm_value=0;
	u8 pwmval;
	u16 adc_value;
	float adc_vol;
	float pwm_vol;
	
	SysTick_Init(72);
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);  //中断优先级分组 分2组
	LED_Init();
	USART1_Init(115200);
	KEY_Init();
	ADCx_Init();
	TIM9_CH2_PWM_Init(255,0);	//TIM11 PWM初始化, Fpwm=168M/256=656.25Khz
	
	while(1)
	{
		key=KEY_Scan(0);
		if(key==KEY_UP_PRESS)
		{
			pwm_value+=10;
			if(pwm_value>=250)
			{
				pwm_value=255;	
			}
			TIM_SetCompare2(TIM9,pwm_value);
		}
		else if(key==KEY1_PRESS)
		{
			pwm_value-=10;
			if(pwm_value<10)
			{
				pwm_value=0;	
			}
			TIM_SetCompare2(TIM9,pwm_value);		
		}
		
		i++;
		if(i%20==0)
		{
			LED1=!LED1;
		}
		
		if(i%100==0)
		{
			pwmval=TIM_GetCapture2(TIM9);
			pwm_vol=(float)pwmval*(3.3/256);
			printf("PWM输出电压为：%.3fV\r\n",pwm_vol);
			
			adc_value=Get_ADC_Value(ADC_Channel_5,20);
			adc_vol=(float)adc_value*(3.3/4096);
			printf("ADC检测电压为：%.3fV\r\n",adc_vol);
		}
		delay_ms(10);
	}
}


