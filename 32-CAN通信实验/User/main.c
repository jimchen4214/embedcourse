/*******************************************************************************
*                 
*                 		       普中科技
--------------------------------------------------------------------------------
* 实 验 名		 : CAN通信实验
* 实验说明       : 
* 连接方式       : 
* 注    意		 : CAN驱动在can.c内
*******************************************************************************/

#include "system.h"
#include "SysTick.h"
#include "led.h"
#include "usart.h"
#include "key.h"
#include "can.h"



/*******************************************************************************
* 函 数 名         : main
* 函数功能		   : 主函数
* 输    入         : 无
* 输    出         : 无
*******************************************************************************/
int main()
{	
	u8 i=0,j=0;
	u8 key;
	u8 mode=0;
	u8 res;
	u8 tbuf[8];
	u8 rbuf[8];
	
	SysTick_Init(168);
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);  //中断优先级分组 分2组
	LED_Init();
	USART1_Init(115200);
	KEY_Init();
	CAN1_Mode_Init(CAN_SJW_1tq,CAN_BS2_8tq,CAN_BS1_9tq,4,CAN_Mode_Normal);//500Kbps波特率
	
	while(1)
	{
		key=KEY_Scan(0);
		if(key==KEY_UP_PRESS)  //模式切换
		{
			mode=!mode;
			CAN1_Mode_Init(CAN_SJW_1tq,CAN_BS2_8tq,CAN_BS1_9tq,4,mode);
			if(mode==0)
			{
				printf("Normal Mode\r\n");
			}
			else
			{
				printf("LoopBack Mode\r\n");
			}
		}
		if(key==KEY1_PRESS)  //发送数据
		{
			for(j=0;j<8;j++)
			{
				tbuf[j]=j;
			}
			res=CAN1_Send_Msg(tbuf,8);
			if(res)
			{
				printf("Send Failed!\r\n");
			}
			else
			{
				printf("发送数据：");
				for(j=0;j<8;j++)
				{
					printf("%X  ",tbuf[j]);
				}
				printf("\r\n");
			}
		}
		res=CAN1_Receive_Msg(rbuf);
		if(res)
		{	
			printf("接收数据：");
			for(j=0;j<8;j++)
			{
				printf("%X  ",rbuf[j]);
			}
			printf("\r\n");
		}
		
		i++;
		if(i%20==0)
		{
			LED1=!LED1;
		}
		delay_ms(10);
	}
}


