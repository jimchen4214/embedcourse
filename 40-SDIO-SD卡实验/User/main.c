#include "system.h"
#include "SysTick.h"
#include "led.h"
#include "usart.h"
#include "tftlcd.h" 
#include "malloc.h" 
#include "sdio_sdcard.h" 


int main()
{	
	u8 i=0;
	
	SysTick_Init(168);
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);  //中断优先级分组 分2组
	LED_Init();
	USART1_Init(115200);
	TFTLCD_Init();			//LCD初始化
	
	FRONT_COLOR=RED;//设置字体为红色 
	LCD_ShowString(10,10,tftlcd_data.width,tftlcd_data.height,16,"PRECHIN STM32F4");	
	LCD_ShowString(10,30,tftlcd_data.width,tftlcd_data.height,16,"SD CARD TEST");	
	LCD_ShowString(10,50,tftlcd_data.width,tftlcd_data.height,16,"www.prechin.net");  
	
	
	while(SD_Init())//检测不到SD卡
	{
		LCD_ShowString(10,100,tftlcd_data.width,tftlcd_data.height,16,"SD Card Error!");
		printf("SD Card Error!\r\n");
		delay_ms(500);					
	}
	
 	FRONT_COLOR=BLUE;	//设置字体为蓝色 
	//检测SD卡成功 			
	printf("SD Card OK!\r\n");	
	LCD_ShowString(10,100,tftlcd_data.width,tftlcd_data.height,16,"SD Card OK    ");
	
	printf("SD Card Size: %lldMB\r\n",SDCardInfo.CardCapacity>>20);
	LCD_ShowString(10,120,tftlcd_data.width,tftlcd_data.height,16,"SD Card Size:     MB");
	LCD_ShowNum(10+13*8,120,SDCardInfo.CardCapacity>>20,5,16);//显示SD卡容量
	
	switch(SDCardInfo.CardType)
	{
		case SDIO_STD_CAPACITY_SD_CARD_V1_1:
			printf("Card Type:SDSC V1.1\r\n");
			LCD_ShowString(10,140,tftlcd_data.width,tftlcd_data.height,16,"SD Card Type: SDSC V1.1");
			break;
		case SDIO_STD_CAPACITY_SD_CARD_V2_0:
			printf("Card Type:SDSC V2.0\r\n");
			LCD_ShowString(10,140,tftlcd_data.width,tftlcd_data.height,16,"SD Card Type: SDSC V2.0");
			break;
		case SDIO_HIGH_CAPACITY_SD_CARD:printf("Card Type:SDHC V2.0\r\n");
			LCD_ShowString(10,140,tftlcd_data.width,tftlcd_data.height,16,"SD Card Type: SDHC V2.0");
			break;
		case SDIO_MULTIMEDIA_CARD:
			printf("Card Type:MMC Card\r\n");
			LCD_ShowString(10,140,tftlcd_data.width,tftlcd_data.height,16,"SD Card Type: MMC Card ");
			break;
	}	
	
	
	while(1)
	{
		i++;
		if(i%20==0)
		{
			LED1=!LED1;
		}
		delay_ms(10);
	}
}


