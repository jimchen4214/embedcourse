#include "system.h"
#include "SysTick.h"
#include "led.h"
#include "usart.h"
#include "key.h"
#include "tftlcd.h" 
#include "malloc.h" 
#include "sdio_sdcard.h" 
#include "flash.h"
#include "ff.h" 
#include "fatfs_app.h"
#include "font_show.h"


int main()
{	
	u8 i=0;
	u8 key;
	
	SysTick_Init(168);
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);  //中断优先级分组 分2组
	LED_Init();
	USART1_Init(115200);
	TFTLCD_Init();			//LCD初始化
	KEY_Init();
	EN25QXX_Init();				//初始化EN25Q128	 
	my_mem_init(SRAMIN);		//初始化内部内存池
	
	FRONT_COLOR=RED;//设置字体为红色 
	LCD_ShowString(10,10,tftlcd_data.width,tftlcd_data.height,16,"PRECHIN STM32F4");	
	LCD_ShowString(10,30,tftlcd_data.width,tftlcd_data.height,16,"www.prechin.net");
	LCD_ShowString(10,50,tftlcd_data.width,tftlcd_data.height,16,"Font Test");
	FRONT_COLOR=BLUE;	//设置字体为蓝色
	while(SD_Init())//检测不到SD卡
	{
		LCD_ShowString(10,80,tftlcd_data.width,tftlcd_data.height,16,"SD Card Error!");
		delay_ms(500);
		LED2=!LED2;
		LCD_Fill(10,80,tftlcd_data.width,80+16,BACK_COLOR);
	}
	
	FATFS_Init();				//为fatfs相关变量申请内存				 
  	f_mount(fs[0],"0:",1); 		//挂载SD卡
	f_mount(fs[1],"1:",1); 		//挂载SPI FLASH
	
	while(font_init()) 		        //检查字库
	{
Update:    
		LCD_ShowString(10,80,tftlcd_data.width,tftlcd_data.height,16,"Font Updating...");
		key=update_font(10,100,16,"0:");//更新字库
		while(key)//更新失败		
		{		
			 LCD_ShowString(10,100,tftlcd_data.width,tftlcd_data.height,16,"Font Update Failed! ");
			 delay_ms(200);
		} 		  
		LCD_ShowString(10,100,tftlcd_data.width,tftlcd_data.height,16,"Font Update Success!    ");
		delay_ms(1500);
	} 
	LCD_ShowFontString(10,130,tftlcd_data.width,tftlcd_data.height,"普中科技-PRECHIN",16,0);
	LCD_ShowFontString(10,150,tftlcd_data.width,tftlcd_data.height,"www.prechin.net",16,0);
	LCD_ShowFontString(10,170,tftlcd_data.width,tftlcd_data.height,"字库显示实验",16,0);
	LCD_ShowFontString(10,190,tftlcd_data.width,tftlcd_data.height,"K_UP键进行字库更新...",16,0);
	
	while(1)
	{
		key=KEY_Scan(0);
		if(key==KEY_UP_PRESS)
			goto Update;
		i++;
		if(i%10==0)
		{
			LED1=!LED1;
		}
		delay_ms(10);	
	}
}


