#include "system.h"
#include "SysTick.h"
#include "led.h"
#include "usart.h"
#include "tftlcd.h" 
#include "malloc.h" 
#include "sdio_sdcard.h" 
#include "flash.h"
#include "key.h"
#include "usbd_msc_core.h"
#include "usbd_usr.h"
#include "usbd_desc.h"
#include "usb_conf.h"



USB_OTG_CORE_HANDLE USB_OTG_dev;
extern vu8 USB_STATUS_REG;		//USB状态
extern vu8 bDeviceState;		//USB连接 情况



int main()
{	
	u8 offline_cnt=0;
	u8 tct=0;
	u8 USB_STA;
	u8 Divece_STA;
    
	
	SysTick_Init(168);
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);  //中断优先级分组 分2组
	LED_Init();
	KEY_Init();
	USART1_Init(115200);
	TFTLCD_Init();			//LCD初始化
	EN25QXX_Init();				//初始化EN25Q128	  
	
	my_mem_init(SRAMIN);		//初始化内部内存池
	
	FRONT_COLOR=RED;//设置字体为红色 	
	LCD_ShowString(10,10,tftlcd_data.width,tftlcd_data.height,16,"USB Card Reader TEST");	
	LCD_ShowString(10,30,tftlcd_data.width,tftlcd_data.height,16,"www.prechin.net");
	
	if(SD_Init()!=0)
	{	
		LCD_ShowString(10,50,tftlcd_data.width,tftlcd_data.height,16,"SD Card Error!");	//检测SD卡错误
	}
	else //SD 卡正常
	{   															  
		LCD_ShowString(10,50,tftlcd_data.width,tftlcd_data.height,16,"SD Card Size:     MB"); 
 		LCD_ShowNum(10+13*8,50,SDCardInfo.CardCapacity>>20,5,16);	//显示SD卡容量
 	}
	if(EN25QXX_ReadID()!=EN25Q128)
	{
		LCD_ShowString(10,70,tftlcd_data.width,tftlcd_data.height,16,"EN25Q128 Error!         ");	//检测EN25Q128错误
	}
	else //SPI FLASH 正常
	{   														 
		LCD_ShowString(10,70,tftlcd_data.width,tftlcd_data.height,16,"EN25Q128 FLASH Size:12MB");	 
	}  
 	LCD_ShowString(10,90,tftlcd_data.width,tftlcd_data.height,16,"USB Connecting...");//提示正在建立连接 	    
	printf("USB Connecting...\r\n");
	USBD_Init(&USB_OTG_dev,USB_OTG_FS_CORE_ID,&USR_desc,&USBD_MSC_cb,&USR_cb);
	delay_ms(2000);
	
	while(1)
	{	
		delay_ms(1);				  
		if(USB_STA!=USB_STATUS_REG)//状态改变了 
		{	 						   			  	   
			if(USB_STATUS_REG&0x01)//正在写		  
			{
				LED2=0;
				LCD_ShowString(10,120,tftlcd_data.width,tftlcd_data.height,16,"USB Writing...");//提示USB正在写入数据	 
				printf("USB Writing...\r\n");
			}
			if(USB_STATUS_REG&0x02)//正在读
			{
				LED2=0;
				LCD_ShowString(10,120,tftlcd_data.width,tftlcd_data.height,16,"USB Reading...");//提示USB正在读出数据  		 
				printf("USB Reading...\r\n");
			}	 										  
			if(USB_STATUS_REG&0x04)
				LCD_ShowString(10,140,tftlcd_data.width,tftlcd_data.height,16,"USB Write Err ");//提示写入错误
			else 
				LCD_Fill(10,140,tftlcd_data.width,140+16,WHITE);//清除显示	  
			if(USB_STATUS_REG&0x08)
				LCD_ShowString(10,140,tftlcd_data.width,tftlcd_data.height,16,"USB Read  Err ");//提示读出错误
			else 
				LCD_Fill(10,140,tftlcd_data.width,140+16,WHITE);//清除显示    
			USB_STA=USB_STATUS_REG;//记录最后的状态
		}
		if(Divece_STA!=bDeviceState) 
		{
			if(bDeviceState==1)
			{
				LCD_ShowString(10,90,tftlcd_data.width,tftlcd_data.height,16,"USB Connected    ");//提示USB连接已经建立	
				printf("USB Connected\r\n");
			}
			else 
			{
				LCD_ShowString(10,90,tftlcd_data.width,tftlcd_data.height,16,"USB DisConnected ");//提示USB被拔出了
				printf("USB DisConnected\r\n");
			}
			Divece_STA=bDeviceState;
		}
		tct++;
		if(tct==200)
		{
			tct=0;
			LED2=1;
			LED1=!LED1;//提示系统在运行
			if(USB_STATUS_REG&0x10)
			{
				offline_cnt=0;//USB连接了,则清除offline计数器
				bDeviceState=1;
			}else//没有得到轮询 
			{
				offline_cnt++;  
				if(offline_cnt>10)bDeviceState=0;//2s内没收到在线标记,代表USB被拔出了
			}
			USB_STATUS_REG=0;
		}
	}
}


