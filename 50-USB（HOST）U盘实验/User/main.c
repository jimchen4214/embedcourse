#include "system.h"
#include "SysTick.h"
#include "led.h"
#include "usart.h"
#include "tftlcd.h" 
#include "malloc.h" 
#include "sdio_sdcard.h" 
#include "flash.h"
#include "key.h"
#include "fatfs_app.h"
#include "ff.h"	
#include "usbh_usr.h" 



USBH_HOST  USB_Host;
USB_OTG_CORE_HANDLE  USB_OTG_Core;


//用户测试主程序
//返回值:0,正常
//       1,有问题
u8 USH_User_App(void)
{ 
	u32 total,free;
	u8 res=0;
	LCD_ShowString(10,140,tftlcd_data.width,tftlcd_data.height,16,"USB Connect OK!   ");	 
	res=fatfs_getfree("2:",&total,&free);
	if(res==0)
	{
		FRONT_COLOR=BLUE;//设置字体为蓝色	   
		LCD_ShowString(10,160,tftlcd_data.width,tftlcd_data.height,16,"FATFS OK!");	
		LCD_ShowString(10,180,tftlcd_data.width,tftlcd_data.height,16,"U Disk Total Size:     MB");	 
		LCD_ShowString(10,200,tftlcd_data.width,tftlcd_data.height,16,"U Disk  Free Size:     MB"); 	    
		LCD_ShowNum(10+18*8,180,total>>10,5,16); //显示U盘总容量 MB
		LCD_ShowNum(10+18*8,200,free>>10,5,16);	
	} 
 
	while(HCD_IsDeviceConnected(&USB_OTG_Core))//设备连接成功
	{	
		LED2=!LED2;
		delay_ms(200);
	}
	FRONT_COLOR=RED;//设置字体为红色	   
	LCD_ShowString(10,140,tftlcd_data.width,tftlcd_data.height,16,"USB Connectting...");
	LCD_Fill(10,160,239,220,WHITE);
	return res;
} 


int main()
{	
	u8 i;
    
	SysTick_Init(168);
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);  //中断优先级分组 分2组
	LED_Init();
	KEY_Init();
	USART1_Init(115200);
	TFTLCD_Init();			//LCD初始化
	EN25QXX_Init();				//初始化EN25Q128	  
	my_mem_init(SRAMIN);		//初始化内部内存池
	FATFS_Init();
	f_mount(fs[0],"0:",1); 	//挂载SD卡  
  	f_mount(fs[1],"1:",1); 	//挂载flash  
  	f_mount(fs[2],"2:",1); 	//挂载U盘
	
	FRONT_COLOR=RED;//设置字体为红色 	
	LCD_ShowString(10,10,tftlcd_data.width,tftlcd_data.height,16,"USB UPan TEST");	
	LCD_ShowString(10,30,tftlcd_data.width,tftlcd_data.height,16,"www.prechin.net");
	
	//初始化USB主机
  	USBH_Init(&USB_OTG_Core,USB_OTG_FS_CORE_ID,&USB_Host,&USBH_MSC_cb,&USR_Callbacks);
	
	while(1)
	{	
		USBH_Process(&USB_OTG_Core, &USB_Host);
		
		i++;
		if(i%20==0)
		{
			LED1=!LED1;
		}
		delay_ms(10);
	}
}


