#include "system.h"
#include "SysTick.h"
#include "led.h"
#include "usart.h"
#include "tftlcd.h" 
#include "usbh_usr.h" 
#include "string.h"



USBH_HOST  USB_Host;
USB_OTG_CORE_HANDLE  USB_OTG_Core_dev;
extern HID_Machine_TypeDef HID_Machine;


//USB信息显示
//msgx:0,USB无连接
//     1,USB键盘
//     2,USB鼠标
//     3,不支持的USB设备
void USBH_Msg_Show(u8 msgx)
{
	FRONT_COLOR=RED;
	switch(msgx)
	{
		case 0:	//USB无连接
			LCD_ShowString(10,130,200,16,16,"USB Connecting...");	
			LCD_Fill(0,150,tftlcd_data.width,tftlcd_data.height,WHITE);
			break;
		case 1:	//USB键盘
			LCD_ShowString(10,130,200,16,16,"USB Connected    ");	
			LCD_ShowString(10,150,200,16,16,"USB KeyBoard");	 
			LCD_ShowString(10,180,210,16,16,"KEYVAL:");	
			LCD_ShowString(10,200,210,16,16,"INPUT STRING:");	
			break;
		case 2:	//USB鼠标
			LCD_ShowString(10,130,200,16,16,"USB Connected    ");	
			LCD_ShowString(10,150,200,16,16,"USB Mouse");	 
			LCD_ShowString(10,180,210,16,16,"BUTTON:");	
			LCD_ShowString(10,200,210,16,16,"X POS:");	
			LCD_ShowString(10,220,210,16,16,"Y POS:");	
			LCD_ShowString(10,240,210,16,16,"Z POS:");	
			break; 		
		case 3:	//不支持的USB设备
			LCD_ShowString(10,130,200,16,16,"USB Connected    ");	
			LCD_ShowString(10,150,200,16,16,"Unknow Device");	 
			break; 	 
	} 
}  

//HID重新连接
void USBH_HID_Reconnect(void)
{
	//关闭之前的连接
	USBH_DeInit(&USB_OTG_Core_dev,&USB_Host);	//复位USB HOST
	USB_OTG_StopHost(&USB_OTG_Core_dev);		//停止USBhost
	if(USB_Host.usr_cb->DeviceDisconnected)		//存在,才禁止
	{
		USB_Host.usr_cb->DeviceDisconnected(); 	//关闭USB连接
		USBH_DeInit(&USB_OTG_Core_dev, &USB_Host);
		USB_Host.usr_cb->DeInit();
		USB_Host.class_cb->DeInit(&USB_OTG_Core_dev,&USB_Host.device_prop);
	}
	USB_OTG_DisableGlobalInt(&USB_OTG_Core_dev);//关闭所有中断
	//重新复位USB
	RCC_AHB2PeriphClockCmd(RCC_AHB2Periph_OTG_FS,ENABLE);//USB OTG FS 复位
	delay_ms(50);
	RCC_AHB2PeriphClockCmd(RCC_AHB2Periph_OTG_FS,DISABLE);	//复位结束  

	memset(&USB_OTG_Core_dev,0,sizeof(USB_OTG_CORE_HANDLE));
	memset(&USB_Host,0,sizeof(USB_Host));
	//重新连接USB HID设备
	USBH_Init(&USB_OTG_Core_dev,USB_OTG_FS_CORE_ID,&USB_Host,&HID_cb,&USR_Callbacks);  
}


int main()
{	
	u32 i=0;

	SysTick_Init(168);
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);  //中断优先级分组 分2组
	USART1_Init(115200);
	LED_Init();
	TFTLCD_Init();			//LCD初始化
	FRONT_COLOR=RED;
	LCD_ShowString(10,10,tftlcd_data.width,tftlcd_data.height,16,"USB MOUSE/KEYBOARD TEST");
	LCD_ShowString(10,30,tftlcd_data.width,tftlcd_data.height,16,"www.prechin.cn");
  	USBH_Init(&USB_OTG_Core_dev,USB_OTG_FS_CORE_ID,&USB_Host,&HID_cb,&USR_Callbacks);
	while(1)
	{
		/* Host Task handler */
		USBH_Process(&USB_OTG_Core_dev , &USB_Host);
		
		if(bDeviceState==1)//连接建立了
		{ 
			
			if(USBH_Check_HIDCommDead(&USB_OTG_Core_dev,&HID_Machine))//检测USB HID通信,是否还正常? 
			{ 	    
				USBH_HID_Reconnect();//重连
			}					
		}
		else	//连接未建立的时候,检测
		{
			if(USBH_Check_EnumeDead(&USB_Host))	//检测USB HOST 枚举是否死机了?死机了,则重新初始化 
			{ 	    
				USBH_HID_Reconnect();//重连
			}		
		}
		i++;
		if(i%300000==0)
		{
			LED1=!LED1;
		}
//		delay_ms(10);		//过长延时将导致数据接收不到，一直在中断
	}
}


