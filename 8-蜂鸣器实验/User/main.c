/*******************************************************************************
*                 
*                 		       ????
--------------------------------------------------------------------------------
* ? ? ?		 : ?????
* ????       : 
* ????       : 
* ?    ?		 : ????????beep.c?
*******************************************************************************/

#include "system.h"
#include "SysTick.h"
#include "led.h"
#include "beep.h"

/*******************************************************************************
* ? ? ?         : main
* ????		   : ???
* ?    ?         : ?
* ?    ?         : ?
*******************************************************************************/
/*
int main() {
    SysTick_Init(168);
    LED_Init();
    BEEP_Init();

    while(1) {
        LED1 = !LED1;
        BEEP = 1; // Turn BEEP on
        delay_ms(10); // Keep it on for 'high_time' milliseconds

        BEEP = 0; // Turn BEEP off
        delay_ms(10); // Keep it off for 'low_time' milliseconds
    }
}
*/


int main() {
    SysTick_Init(168);
    LED_Init();
    BEEP_Init();
  
    while(1) {
        LED1 = !LED1;
        BEEP = 1; // Turn BEEP on
        delay_ms(1); // Keep it on briefly

        BEEP = 0; // Turn BEEP off
        delay_ms(5); // Keep it off longer
    }
}

